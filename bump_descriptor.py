#! /usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import subprocess
from tempfile import TemporaryDirectory
from pathlib import Path
from datetime import datetime

# Current dir.
CWD = Path(__file__).parent.absolute()

# run date
NOW = datetime.now()

# string format for datetime
TIME_OUT_STRFORMAT = "%Y%m%dT%H%M%S"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--modification",
        nargs=1,
        type=str,
        default=[None],
        help="Message to save in descriptor.release.modification",
    )
    parser.add_argument(
        "--force", action="store_true", default=False, help="Force bump"
    )
    parser.add_argument(
        "-s",
        "--build-setup",
        action="store_true",
        default=False,
        help="Generate setup.py file with dephell",
    )
    args = parser.parse_args()

    from pathlib import Path
    import json
    from datetime import datetime
    import toml

    root_dir = Path(__file__).parent

    metadata = toml.load(root_dir / "pyproject.toml")["tool"]["poetry"]
    version = metadata["version"]

    descriptor_path = root_dir / "poppy" / "pop" / "descriptor.json"

    with open(descriptor_path, "r") as json_file:
        data = json.load(json_file)

    if version == data["release"]["version"]:
        print(f"Current descriptor version ({version}) is already the last one")
        if not args.force:
            return

    print(f"Bumping descriptor from {data['release']['version']} to {version}")

    print("Replacing following fields: version, date and modification")

    data["release"]["author"] = ", ".join(metadata["authors"])
    data["release"]["version"] = version
    data["release"]["date"] = datetime.today().strftime("%Y-%m-%d")
    if args.modification[0]:
        modification = args.modification[0]
    else:
        modification = f"Bump the plugin to version {version}"
    data["release"]["modification"] = modification
    data["release"]["url"] = metadata["repository"]

    with open(descriptor_path, "w") as json_file:
        json.dump(data, json_file, indent=2)

    print("Done.")

    if args.build_setup:
        print("Generating setup.py file ...")
        Path(CWD / "setup.py").unlink(missing_ok=True)
        venv_temp_dir = (
            Path(TemporaryDirectory().name)
            / f"{CWD.name}_dephell_{NOW.strftime(TIME_OUT_STRFORMAT)}"
        )
        venv_temp_bin = venv_temp_dir / "bin" / "activate"
        cmd = [f"python3 -m venv {venv_temp_dir} &&"]
        cmd.append(f"source {venv_temp_bin} &&")
        cmd.append("pip install dephell &&")
        cmd.append("dephell deps convert &&")
        cmd.append("deactivate &&")
        cmd.append(f"rm -rf {venv_temp_dir}")
        cmd = " ".join(cmd)
        try:
            completed = subprocess.run(cmd, shell=True, cwd=str(CWD))
        except subprocess.CalledProcessError as e:
            print(f"Generating setup.py has failed:\n{e}")
            print(f"{completed.stderr}")
        except Exception as e:
            print(f"Generating setup.py has failed:\n{e}")
        else:
            print(f"{completed.stdout}")

        print("Done")


if __name__ == "__main__":
    main()
