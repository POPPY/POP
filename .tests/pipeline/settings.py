#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
POPPy settings.
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
ROOT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

# POPPy database
MAIN_DATABASE = "MAIN-DB"

# RODP pipeline database
PIPELINE_DATABASE = "MAIN-DB"

# MUSIC database
MUSIC_DATABASE = PIPELINE_DATABASE

# the test database
TEST_DATABASE = MAIN_DATABASE

PLUGINS = [
    "poppy.pop",
]

# load the logger config
logger_json_filepath = os.path.join(ROOT_DIRECTORY, "logger.json")
if os.path.isfile(logger_json_filepath):
    import collections
    import json

    with open(logger_json_filepath, "rt") as json_file:
        LOGGER_CONFIG = json.load(json_file, object_pairs_hook=collections.OrderedDict)
