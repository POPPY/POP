#!/bin/bash

set -e

psql -h $Host -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    DROP DATABASE IF EXISTS db_poppy_test;
EOSQL
