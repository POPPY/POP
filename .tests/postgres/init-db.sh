#!/bin/bash

set -e

psql -h $Host -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER "popuser" PASSWORD 'popnroll' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT;
    CREATE USER "popadmin" PASSWORD 'popnroll' NOSUPERUSER CREATEDB NOCREATEROLE INHERIT;
    CREATE DATABASE db_poppy_test WITH OWNER popadmin;
    GRANT CONNECT ON DATABASE db_poppy_test TO popuser;
    GRANT ALL PRIVILEGES ON DATABASE db_poppy_test TO popadmin;
EOSQL
