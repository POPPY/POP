-- Remove recursively the schema of the migrator
DROP SCHEMA IF EXISTS migrator CASCADE;

-- commit
COMMIT;
