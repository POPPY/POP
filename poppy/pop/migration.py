#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__all__ = ["MigrateError"]


class MigrateError(Exception):
    """
    Errors linked to the migration.
    """
